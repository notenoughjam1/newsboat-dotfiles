This is a sample of my dotfiles for newsboat, with examples of query feeds, macros, media RSS feeds (Odysee + YouTube + Podcasts), and blog RSS feeds. Take it and use it however you like.

I've updated the RSS feeds for Odysee channels with the official Odysee RSS feed, as https://lbryfeed.melroy.org was down periodically. Odysee's Official RSS Feed should be more reliable. Theoretically, however, lbryfeed would be better because it would work for any LBRY frontend—not just Odysee. 

Here's how you get the RSS feed for a channel on Odysee:

![Copy RSS Feed URL for Odysee Channel](rss-for-odysee.png "Copy RSS Feed URL for Odysee Channel")
